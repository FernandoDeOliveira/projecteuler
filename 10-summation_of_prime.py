def get_primes(n):
    from math import ceil, sqrt

    primelist = []
    for number in range(2, n+1):
        is_prime = True
        root = int(ceil(sqrt(number)))
        for prime in primelist:
            if prime > root:
                    break
            if number % prime == 0:
                is_prime = False
                break
        if is_prime:
            primelist.append(number)
    return primelist


N = int(2e6)
array_primes = get_primes(N)
sum_primes = sum(array_primes)
#print(array_primes)
print(sum_primes)
