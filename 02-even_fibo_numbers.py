N = 4000000

def fibonacci(N):
    """Return all fibonacci numbers up to N. """
    a, b = 1, 1
    while a <= N:
        yield a
        a, b = b, a + b
# SUM OF EVEN NUMBERS IN FIBONACCI SEQUENCE
print(sum(f for f in fibonacci(N) if not f % 2))
